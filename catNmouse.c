///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Brayden Suzuki <braydens@hawaii.edu>
/// @date    23_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#define DEFAULT_MAX_NUMBER (2048)

int main( int argc, char* argv[] ) {
   int theMaxValue ;
   printf( "Cat `n Mouse\n" );
   if (argc > 1) {
      theMaxValue = atoi(argv[1]);
      if (theMaxValue < 1) {
         exit ( 1 ) ;
      }
   }
   else {
      theMaxValue = DEFAULT_MAX_NUMBER;
   }
   printf( "Set maximum number is: %d\n", theMaxValue );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   int rand_num = rand() % (theMaxValue - 1) ;
   int aGuess ;
   printf( "OK cat, I'm thinking of a number from 1 to %d.\n", theMaxValue ) ;
   while (rand_num != aGuess) {
      printf( "Make a guess: " );
      scanf( "%d", &aGuess );
      if (aGuess < 1) {
         printf( "You must enter a number that's >= 1\n" ) ;
      }
      else if(aGuess > theMaxValue) {
         printf( "You must enter a number that's <= %d\n", theMaxValue );
      }
      else {
      if (rand_num != aGuess) {
         if (rand_num < aGuess) {
            printf( "No cat... the number I'm thinking of is smaller than %d\n", aGuess );
         }
         else {
         printf( "No cat... the number I'm thinking of is larger than %d\n", aGuess );
         }
      }
      else {
         printf( "You got me.\n" );
         printf( " _._     _,-'""`-._\n" );
         printf( "(,-.`._,'(       |\\`-/|\n" );
         printf( "    `-.-' \\ )-`( , o o)\n" );
         printf( "          `-    \\`_`''-\n" );
      }
   }
   }
   return 0;  // This is an example of how to return a 1
}





